# renato-wiki modules showcase

## Plugins

### Official

All plugins by the renato-wiki team can be found in
[subgroups on gitlab](https://gitlab.com/groups/renato-wiki/subgroups).

### 3rd party

There are no known 3rd party plugins yet. See below to add yours.

<!--

**[AUTHOR](WEBSITE):**

 * `PACKAGE_NAME` \[[npm](https://www.npmjs.com/package/PACKAGE_NAME), [source](SOURCE_URL)\] - Short description of the plugin.
 * [...]

-->

## Themes

### Official

#### Nerissa

| key | value |
|---|---|
| author | [frissdiegurke]() |
| npm | `@renato-wiki/theme-nerissa` \[[link](https://www.npmjs.com/package/@renato-wiki/theme-nerissa)\] |
| source | https://gitlab.com/renato-wiki/themes/nerissa |

An artless and lightweight theme with blue colors.

<!-- TODO add screenshot -->

### 3rd party

There are no known 3rd party themes yet. See below to add yours.

## Add your modules

Send a [merge request](https://gitlab.com/renato-wiki/module-showcase/merge_requests/new) in order to add your modules!

Please keep your entries tidy and clean-up outdated information. Thanks!

### Plugins

You can see a schema entry below the list. Just use it to create your entries.

### Themes

Just add another sub-heading with your theme summary. - Screenshots must not have a size greater than `720x480`.
